# DKX/WaitPromise

Wait promise for node.js

## Installation

```bash
$ npm install --save @dkx/wait-promise
```

or with yarn

```bash
$ yarn add @dkx/wait-promise
```

## Usage

```typescript
import {waitPromise} from '@dkx/wait-promise';

(async () => {
	await waitPromise(100);
	console.log('Waited for 100ms');
})();
```
