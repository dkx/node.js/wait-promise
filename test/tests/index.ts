import {expect} from 'chai';
import {performance} from 'perf_hooks';
import {waitPromise} from '../../lib';


describe('waitPromise()', () => {

	it('should wait for given ms', async () => {
		const started = performance.now();
		await waitPromise(100);
		const ended = performance.now();

		expect(ended - started).to.be.greaterThan(99);
	});

});
